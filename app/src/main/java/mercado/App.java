/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package mercado;

import mercado.GUI_mercado.VentanaPrincipal;


public class App {
    
    public static void main(String[] args) {
        VentanaPrincipal vPrincipal = new VentanaPrincipal();
        vPrincipal.setVisible(true);
        vPrincipal.setLocationRelativeTo(null);
        vPrincipal.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    }
}
