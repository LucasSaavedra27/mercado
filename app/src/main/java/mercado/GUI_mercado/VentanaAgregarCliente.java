/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package mercado.GUI_mercado;


import javax.swing.JOptionPane;
import mercado.Mercado;
import mercado.cliente.ClienteExistenteException;
import mercado.cliente.Quintero;
import mercado.cliente.Empresa;

/**
 *
 * @author LUCAS
 */
public class VentanaAgregarCliente extends javax.swing.JFrame {

    private Mercado mercado;
    
    public VentanaAgregarCliente() {
        this.mercado=Mercado.instancia();
        initComponents();
        direccionTF.setEnabled(false);
        correoTF.setEnabled(false);
        telefonoTF.setEnabled(false);
        cuitTF.setEnabled(false);
        rsocialTF.setEnabled(false);
        nombreTF.setEnabled(false);
        apellidoTF.setEnabled(false);
        dniTF.setEnabled(false);
        btnAgregar.setEnabled(false);
    }

    private void initComponents() {

        nombreTF = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cBox = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        apellidoTF = new javax.swing.JTextField();
        dniTF = new javax.swing.JTextField();
        telefonoTF = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        correoTF = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        direccionTF = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        rsocialTF = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        cuitTF = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        btnVolver = new javax.swing.JButton();
        btnAgregar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel2.setText("NOMBRE");

        cBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR", "QUINTERO", "EMPRESA" }));
        cBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cBoxActionPerformed(evt);
            }
        });

        jLabel3.setText("TIPO DE CLIENTE");

        jLabel4.setText("DNI");

        jLabel5.setText("APELLIDO");

        jLabel6.setText("TELÉFONO");

        jLabel7.setText("CORREO");

        jLabel8.setText("DIRECCIÓN");

        jLabel9.setText("RAZÓN SOCIAL");

        jLabel10.setText("CUIT");

        btnVolver.setText("VOLVER");
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        btnAgregar.setText("AGREGAR");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 204, 204));

        jLabel11.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel11.setText("AGREGAR CLIENTE");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addGap(136, 136, 136))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnVolver)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAgregar))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel8)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(57, 57, 57)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cBox, 0, 139, Short.MAX_VALUE)
                            .addComponent(nombreTF)
                            .addComponent(dniTF, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(direccionTF, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(correoTF, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(telefonoTF)
                            .addComponent(apellidoTF, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(rsocialTF, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                            .addComponent(cuitTF))))
                .addGap(79, 79, 79))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(cBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nombreTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(apellidoTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dniTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(direccionTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(correoTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(telefonoTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cuitTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rsocialTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnVolver)
                    .addComponent(btnAgregar))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {
        this.dispose();
    }

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {
        String direccion=direccionTF.getText();
        String correo=correoTF.getText();
        String telefono=telefonoTF.getText();
        if(cBox.getSelectedItem().equals("QUINTERO")){
            String nombre=nombreTF.getText();
            String apellido=apellidoTF.getText();
            String dni=dniTF.getText();
            Quintero quintero=new Quintero(direccion,correo,telefono,nombre,apellido,dni);
            try {
                mercado.getClientes().agregarCliente(quintero);
                JOptionPane.showMessageDialog(null,"INGRESO EXITOSO", "INGRESO CORRECTO", JOptionPane.INFORMATION_MESSAGE);
                vaciarTF();
            } catch (ClienteExistenteException ex) {
                JOptionPane.showMessageDialog(null,"CLIENTE EXISTENTE", "ERROR", JOptionPane.ERROR_MESSAGE);
                vaciarTF();
            }
        }
        if(cBox.getSelectedItem().equals("EMPRESA")){
            String cuit=cuitTF.getText();
            String rsocial=rsocialTF.getText();
            Empresa empresa = new Empresa(direccion,correo,telefono,cuit,rsocial);
            try {
                mercado.getClientes().agregarCliente(empresa);
                JOptionPane.showMessageDialog(null,"INGRESO EXITOSO", "INGRESO CORRECTO", JOptionPane.INFORMATION_MESSAGE);
                vaciarTF();
            } catch (ClienteExistenteException ex) {
                JOptionPane.showMessageDialog(null,"CLIENTE EXISTENTE", "ERROR", JOptionPane.ERROR_MESSAGE);
                vaciarTF();
            }
        }
        
    }

    private void vaciarTF(){
    direccionTF.setText("");
    correoTF.setText("");
    telefonoTF.setText("");
    cuitTF.setText("");
    rsocialTF.setText("");
    nombreTF.setText("");
    apellidoTF.setText("");
    dniTF.setText("");
    }
    
    private void cBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cBoxActionPerformed
        direccionTF.setEnabled(true);
        correoTF.setEnabled(true);
        telefonoTF.setEnabled(true);
        if(cBox.getSelectedItem().equals("QUINTERO")){
            vaciarTF();
            cuitTF.setEnabled(false);
            rsocialTF.setEnabled(false);
            nombreTF.setEnabled(true);
            apellidoTF.setEnabled(true);
            dniTF.setEnabled(true);
            btnAgregar.setEnabled(true);
        }
        if(cBox.getSelectedItem().equals("EMPRESA")){
            vaciarTF();
            cuitTF.setEnabled(true);
            rsocialTF.setEnabled(true);
            nombreTF.setEnabled(false);
            apellidoTF.setEnabled(false);
            dniTF.setEnabled(false);
            btnAgregar.setEnabled(true);
        }if(cBox.getSelectedItem().equals("-- SELECCIONAR")){
            direccionTF.setEnabled(false);
            correoTF.setEnabled(false);
            telefonoTF.setEnabled(false);
            cuitTF.setEnabled(false);
            rsocialTF.setEnabled(false);
            nombreTF.setEnabled(false);
            apellidoTF.setEnabled(false);
            dniTF.setEnabled(false);
            btnAgregar.setEnabled(false);
            vaciarTF();
        }
    }


    private javax.swing.JTextField apellidoTF;
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnVolver;
    private javax.swing.JComboBox<String> cBox;
    private javax.swing.JTextField correoTF;
    private javax.swing.JTextField cuitTF;
    private javax.swing.JTextField direccionTF;
    private javax.swing.JTextField dniTF;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField nombreTF;
    private javax.swing.JTextField rsocialTF;
    private javax.swing.JTextField telefonoTF;
    
}
