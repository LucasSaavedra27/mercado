/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package mercado.GUI_mercado;



import javax.swing.JOptionPane;
import mercado.Mercado;
import mercado.cliente.Cliente;
import mercado.cliente.ClienteInexistenteException;
import mercado.cliente.Empresa;
import mercado.cliente.Quintero;

/**
 *
 * @author LUCAS
 */
public class VentanaBuscar extends javax.swing.JFrame {

    private Mercado mercado;
    public VentanaBuscar() {
        this.mercado=Mercado.instancia();
        initComponents();
    }

    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        cuitDniTF = new javax.swing.JTextField();
        btnVolver = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cBox = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel2.setText("INGRESE CUIT/DNI ");

        cuitDniTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cuitDniTFActionPerformed(evt);
            }
        });

        btnVolver.setText("VOLVER");
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 204, 204));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel1.setText("BUSCAR CLIENTE");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        cBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR", "VISUALIZAR", "ACTUALIZAR" }));
        cBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(btnVolver))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cBox, 0, 164, Short.MAX_VALUE)
                    .addComponent(cuitDniTF))
                .addGap(6, 6, 6))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cuitDniTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnVolver)
                    .addComponent(cBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        pack();
    }

    private void cuitDniTFActionPerformed(java.awt.event.ActionEvent evt) {
        
    }

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {
        this.dispose();
    }

    private void cBoxActionPerformed(java.awt.event.ActionEvent evt) {
        if(cBox.getSelectedItem().equals("VISUALIZAR")){
            String identificador=cuitDniTF.getText();
        try {
            mercado.getClientes().buscarCliente(identificador);
            VentanaMostrarClientes vMostrarClientes=new VentanaMostrarClientes();
            vMostrarClientes.setVisible(true);
            vMostrarClientes.setLocationRelativeTo(null);
            vMostrarClientes.cargarTablaBusqueda(cuitDniTF.getText());  
        } catch (ClienteInexistenteException ex) {
            JOptionPane.showMessageDialog(null,"CLIENTE INEXISTENTE", "ERROR", JOptionPane.ERROR_MESSAGE);  
        }
        cuitDniTF.setText("");
        }
        if(cBox.getSelectedItem().equals("ACTUALIZAR")){
            String identificador=cuitDniTF.getText();
            try {
                Cliente cliente=mercado.getClientes().buscarCliente(identificador);
                if(cliente instanceof Quintero){
                    VentanaUpdateQuintero vUpdatequintero=new VentanaUpdateQuintero(cliente);
                    vUpdatequintero.setVisible(true);
                    vUpdatequintero.setLocationRelativeTo(null);
                }       
            } catch (ClienteInexistenteException ex) {
                JOptionPane.showMessageDialog(null,"CLIENTE INEXISTENTE", "ERROR", JOptionPane.ERROR_MESSAGE);  
            }
                String identificadorEmpresa=cuitDniTF.getText();
                Cliente cliente2;
            try {
                cliente2 = mercado.getClientes().buscarCliente(identificadorEmpresa);
                if(cliente2 instanceof Empresa){
                    VentanaUpdateEmpresa vUpdateEmpresa=new VentanaUpdateEmpresa(cliente2);
                    vUpdateEmpresa.setVisible(true);
                    vUpdateEmpresa.setLocationRelativeTo(null);
                }
            } catch (ClienteInexistenteException ex) {
                JOptionPane.showMessageDialog(null,"CLIENTE INEXISTENTE", "ERROR", JOptionPane.ERROR_MESSAGE);  
            }
                
            cuitDniTF.setText("");
        }
    }

    private javax.swing.JButton btnVolver;
    private javax.swing.JComboBox<String> cBox;
    private javax.swing.JTextField cuitDniTF;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    
}
