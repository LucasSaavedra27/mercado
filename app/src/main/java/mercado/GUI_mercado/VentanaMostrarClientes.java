
package mercado.GUI_mercado;

import javax.swing.table.DefaultTableModel;
import mercado.Mercado;
import mercado.cliente.Cliente;
import mercado.cliente.Empresa;
import mercado.cliente.Quintero;

public class VentanaMostrarClientes extends javax.swing.JFrame {

    private Mercado mercado;
    public VentanaMostrarClientes() {
        this.mercado=Mercado.instancia();
        initComponents();
    }
    
     public void cargarTablaCompleta() {
        DefaultTableModel mt=(DefaultTableModel) tabla.getModel();
        Object [] fila = new Object [9];
        for(Cliente cliente : mercado.getClientes().getClientes()){
            if(cliente instanceof Quintero){
                fila[0]="QUINTERO";
                fila[1]=((Quintero) cliente).getDni();
                fila[2]=((Quintero) cliente).getNombre();
                fila[3]=((Quintero) cliente).getApellido();
                fila[4]=null;
                fila[5]=null;
                fila[6]=cliente.getDireccion();
                fila[7]=cliente.getMail();
                fila[8]=cliente.getTelefono();          
            }else if(cliente instanceof Empresa){
                fila[0]="EMPRESA";
                fila[1]=null;
                fila[2]=null;
                fila[3]=null;
                fila[4]=((Empresa) cliente).getCuit();
                fila[5]=((Empresa) cliente).getRazonSocial();
                fila[6]=cliente.getDireccion();
                fila[7]=cliente.getMail();
                fila[8]=cliente.getTelefono();
            }
            mt.addRow(fila);  
        }
        tabla.setModel(mt);
    }
     
      public void cargarTablaBusqueda(String identificador) {
        DefaultTableModel mt=(DefaultTableModel) tabla.getModel();
        Object [] fila = new Object [9];
        for(Cliente cliente : mercado.getClientes().getClientes()){
            if(cliente.getIdentificador().equals(identificador)){
                if(cliente instanceof Quintero){
                   fila[0]="QUINTERO";
                   fila[1]=((Quintero) cliente).getDni();
                   fila[2]=((Quintero) cliente).getNombre();
                   fila[3]=((Quintero) cliente).getApellido();
                   fila[4]=null;
                   fila[5]=null;
                   fila[6]=cliente.getDireccion();
                   fila[7]=cliente.getMail();
                   fila[8]=cliente.getTelefono();          
               }else if(cliente instanceof Empresa){
                   fila[0]="EMPRESA";
                   fila[1]=null;
                   fila[2]=null;
                   fila[3]=null;
                   fila[4]=((Empresa) cliente).getCuit();
                   fila[5]=((Empresa) cliente).getRazonSocial();
                   fila[6]=cliente.getDireccion();
                   fila[7]=cliente.getMail();
                   fila[8]=cliente.getTelefono();
               }
               mt.addRow(fila); 
            }
        }
        tabla.setModel(mt);
    }

    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        btnVolver = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TIPO CLIENTE", "DNI", "NOMBRE", "APELLIDO", "CUIT", "RAZÓN SOCIAL", "DIRECCIÓN", "CORREO", "TELÉFONO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabla);

        jPanel1.setBackground(new java.awt.Color(0, 204, 204));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel2.setText("LISTADO DE CLIENTES");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel2)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        btnVolver.setText("VOLVER");
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(btnVolver)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1180, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 542, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnVolver)
                .addContainerGap())
        );

        pack();
    }

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        this.dispose();
    }

    private javax.swing.JButton btnVolver;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabla;
    
}
