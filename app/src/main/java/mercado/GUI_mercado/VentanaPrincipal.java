
package mercado.GUI_mercado;

public class VentanaPrincipal extends javax.swing.JFrame {

    
    public VentanaPrincipal() {
        initComponents();
    }

    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuAgregar = new javax.swing.JMenuItem();
        menuEliminar = new javax.swing.JMenuItem();
        menuBuscar = new javax.swing.JMenuItem();
        menuMostrar = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 204, 204));

        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\saave\\Downloads\\clientes.jpg")); 

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 381, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(82, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jMenu1.setText("Clientes");

        menuAgregar.setText("Agregar");
        menuAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAgregarActionPerformed(evt);
            }
        });
        jMenu1.add(menuAgregar);

        menuEliminar.setText("Eliminar");
        menuEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEliminarActionPerformed(evt);
            }
        });
        jMenu1.add(menuEliminar);

        menuBuscar.setText("Buscar");
        menuBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBuscarActionPerformed(evt);
            }
        });
        jMenu1.add(menuBuscar);

        menuMostrar.setText("Mostrar");
        menuMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMostrarActionPerformed(evt);
            }
        });
        jMenu1.add(menuMostrar);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }

    private void menuAgregarActionPerformed(java.awt.event.ActionEvent evt) {
        VentanaAgregarCliente vAgregarCliente = new VentanaAgregarCliente();
        vAgregarCliente.setVisible(true);
        vAgregarCliente.setLocationRelativeTo(null);
    }

    private void menuMostrarActionPerformed(java.awt.event.ActionEvent evt) {
        VentanaMostrarClientes vMostrarClientes=new VentanaMostrarClientes();
        vMostrarClientes.setVisible(true);
        vMostrarClientes.setLocationRelativeTo(null);
        vMostrarClientes.cargarTablaCompleta();
    }

    private void menuBuscarActionPerformed(java.awt.event.ActionEvent evt) {
        VentanaBuscar vBuscar=new VentanaBuscar();
        vBuscar.setVisible(true);
        vBuscar.setLocationRelativeTo(null);
        
    }

    private void menuEliminarActionPerformed(java.awt.event.ActionEvent evt) {
        VentanaEliminar vEliminar=new VentanaEliminar();
        vEliminar.setVisible(true);
        vEliminar.setLocationRelativeTo(null); 
    }

    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JMenuItem menuAgregar;
    private javax.swing.JMenuItem menuBuscar;
    private javax.swing.JMenuItem menuEliminar;
    private javax.swing.JMenuItem menuMostrar;

}
