package mercado;

import mercado.cliente.RepositorioCliente;
import mercado.contrato.RepositorioContrato;
import mercado.lectura.RepositorioMedidores;
import mercado.sector.RepositorioSector;

public class Mercado {
    private RepositorioCliente clientes;
    private RepositorioSector sectores;
    private RepositorioContrato contratos;
    private RepositorioMedidores medidores;
    private static Mercado mercado;
    private Mercado (){
    }

    public static Mercado instancia() {
        if(mercado==null){
            mercado=new Mercado();
        }
        return mercado;
    }

    public RepositorioCliente getClientes() {
    if (this.clientes == null) {
        this.clientes = new RepositorioCliente();
    }
    return this.clientes;
}

    public void setClientes(RepositorioCliente clientes) {
        this.clientes = clientes;
    }

    public RepositorioSector getSectores() {
        return sectores;
    }

    public void setSectores(RepositorioSector sectores) {
        this.sectores = sectores;
    }

    public RepositorioContrato getContratos() {
        return contratos;
    }

    public void setContratos(RepositorioContrato contratos) {
        this.contratos = contratos;
    }

    public RepositorioMedidores getMedidores() {
        return medidores;
    }

    public void setMedidores(RepositorioMedidores medidores) {
        this.medidores = medidores;
    }

    public static Mercado getMercado() {
        return mercado;
    }

    public static void setMercado(Mercado mercado) {
        Mercado.mercado = mercado;
    }
    
}



