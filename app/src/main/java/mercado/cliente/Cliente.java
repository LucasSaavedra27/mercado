package mercado.cliente;

import java.util.ArrayList;
import mercado.contrato.Contrato;

public abstract class Cliente {
    private String direccion;
    private String mail;
    private String telefono;
    private ArrayList<Contrato> contratos;
    
    public Cliente(String direccion, String mail, String telefono) {
        this.direccion = direccion;
        this.mail = mail;
        this.telefono = telefono;
        this.contratos = new ArrayList<Contrato>();
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public abstract String getIdentificador();

    public ArrayList<Contrato> getContratos() {
        return contratos;
    }

    public void setContratos(ArrayList<Contrato> contratos) {
        this.contratos = contratos;
    }

    public void agregarContrato(Contrato contrato) throws ContratoExistenteException{
        if (ContratoExistente(contrato)==false) {
            contratos.add(contrato);
        }else{
            throw new ContratoExistenteException("El contrato nro: "+contrato.getNroContrato()+" ya existe");
        }
    }

    private Boolean ContratoExistente(Contrato contrato){
        for (Contrato contratoAux : contratos) {
            if (contratoAux.equals(contrato)) {
                return true;
            }
        }
        return false;
    }

    public Contrato buscarContrato(int numContrato) throws ContratoInexistenteException{
        for (Contrato contratoAux : contratos) {
            if(contratoAux.getNroContrato().equals(numContrato)){
                return contratoAux;
            }
        }
        throw new ContratoInexistenteException("El contrato nro: "+numContrato+" no existe");
    }

    public void modificarContrato(Contrato nuevoContrato) throws ContratoInexistenteException{
        Contrato contrato = buscarContrato(nuevoContrato.getNroContrato());
        contratos.remove(contrato);
        contratos.add(nuevoContrato);
    }

    public void eliminarContrato(int nroContrato) throws ContratoInexistenteException{
        Contrato contrato = buscarContrato(nroContrato);
        contratos.remove(contrato);
    }
}
