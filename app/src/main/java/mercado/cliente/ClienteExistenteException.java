package mercado.cliente;

public class ClienteExistenteException extends Exception {
    public ClienteExistenteException(String message) {
        super(message);
    }
}
