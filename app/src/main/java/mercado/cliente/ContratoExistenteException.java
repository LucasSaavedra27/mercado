package mercado.cliente;

public class ContratoExistenteException extends Exception{
    public ContratoExistenteException(String message) {
        super(message);
    }
}
