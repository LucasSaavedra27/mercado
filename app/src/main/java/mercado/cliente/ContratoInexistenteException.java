package mercado.cliente;

public class ContratoInexistenteException extends Exception{
    public ContratoInexistenteException(String message) {
        super(message);
    }
}
