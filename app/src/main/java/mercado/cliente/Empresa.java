package mercado.cliente;

import com.google.common.base.Objects;

public class Empresa extends Cliente{
    private String cuit;
    private String razonSocial;
    
    public Empresa(String direccion, String mail, String telefono, String cuit, String razonSocial) {
        super(direccion, mail, telefono);
        this.cuit = cuit;
        this.razonSocial = razonSocial;
    }
    public String getCuit() {
        return cuit;
    }
    public void setCuit(String cuit) {
        this.cuit = cuit;
    }
    public String getRazonSocial() {
        return razonSocial;
    }
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }if (!(o instanceof Empresa)) {
            return false;
        }
        Empresa empresa = (Empresa) o;
        return Objects.equal(this.cuit, empresa.getCuit());
    }


    @Override
    public String getIdentificador() {
        return cuit;
    }


}
