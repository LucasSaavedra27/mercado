package mercado.cliente;

public class Quintero extends Cliente{
    private String nombre;
    private String apellido;
    private String dni; 

    public Quintero(String direccion, String mail, String telefono, String nombre, String apellido, String dni) {
        super(direccion, mail, telefono);
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getDni() {
        return dni;
    }
    public void setDni(String dni) {
        this.dni = dni;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) { //si el objeto que se esta comparando es igual que ingresa por parametro, retorna verdadero
            return true;
        }if (!(o instanceof Quintero)){ // si el parametro no es una instancia de quintero retorna falso
            return false;
        }
        Quintero quintero = (Quintero) o; //castea el objeto por parametro para convertirlo en un Quintero
        return this.dni == quintero.getDni();     //retorna verdadero si son iguales
    }


    @Override
    public String getIdentificador() {
        return dni;
    }
    
}
