package mercado.cliente;

import java.util.ArrayList;


public class RepositorioCliente {
    private ArrayList<Cliente> clientes;

    public RepositorioCliente() {
        this.clientes = new ArrayList<>();
    }

    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(ArrayList<Cliente> clientes) {
        this.clientes = clientes;
    }
    
    public void agregarCliente(Cliente cliente) throws ClienteExistenteException {
        if (clienteExistente(cliente)==false) {
            clientes.add(cliente);
        } else {
            throw new ClienteExistenteException("El cliente ya existe");
        }
    }

    public void eliminarCliente(String identificador) throws ClienteInexistenteException {
        Cliente cliente = buscarCliente(identificador);
        clientes.remove(cliente);
        
    }

    public Cliente buscarCliente(String identificador) throws ClienteInexistenteException {
        for (Cliente cliente : clientes) {
            if(cliente.getIdentificador().equals(identificador)){
                return cliente;
            }     
        }
        throw new ClienteInexistenteException("El cliente no existe");
    }

    public void modificarCliente(Cliente clienteNuevo) throws ClienteInexistenteException {
        Cliente clienteExistente = buscarCliente(clienteNuevo.getIdentificador());
        clientes.remove(clienteExistente);
        clientes.add(clienteNuevo);
    }

    private boolean clienteExistente(Cliente cliente) {
        //return clientes.contains(cliente);
        for (Cliente clienteAux : clientes) {
           if(clienteAux.getIdentificador().equals(cliente.getIdentificador())){
               return true;
           }
        }
        return false;
    }
}

