package mercado.contrato;

import java.time.LocalDate;

import mercado.cliente.Cliente;
import mercado.lectura.Lectura;
import mercado.sector.Puesto;

public class Contrato {
    private Integer nro_contrato;
    private LocalDate fecha_inicio;
    private LocalDate fecha_fin;
    private Double monto_mensual;
    private String responsable;
    private Lectura lectura;
    private Cliente cliente;
    private Puesto puesto;

    public Contrato(Integer nro_contrato, LocalDate fecha_inicio, LocalDate fecha_fin, Double monto_mensual, String responsable, Cliente cliente, Lectura lectura, Puesto puesto) {
        this.nro_contrato = nro_contrato;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.monto_mensual = monto_mensual;
        this.responsable = responsable;
        this.cliente = cliente;
        this.lectura = lectura;
        this.puesto = puesto;
    }

    public Integer getNroContrato() {
        return this.nro_contrato;
    }
    public Cliente getCliente() {
        return cliente;
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setNroContrato(Integer nro_contrato){
        this.nro_contrato = nro_contrato;
    }

    public LocalDate getFechaInicio(){
      return this.fecha_inicio;
    }

    public void setFechaInicio(LocalDate fecha_inicio){
        this.fecha_inicio = fecha_inicio;
    }

    public LocalDate getFechaFin(){
        return this.fecha_fin;
      }
  
      public void setFechaFin(LocalDate fecha_fin){
          this.fecha_fin = fecha_fin;
      }

      public Double getMontoMensual() {
        return this.monto_mensual;
    }

    public void setMontoMensual(Double monto_mensual){
        this.monto_mensual = monto_mensual;
    }

    public String getResponsable(){
        return this.responsable;
    }

    public void setResponsable(String responsable){
        this.responsable = responsable;
    }

    public void setLectura(Lectura lectura){
        this.lectura = lectura;
    }

    public Lectura getLectura(){
        return lectura;
    }

    public boolean equals(Object o){
        Contrato toCompare = (Contrato) o;
        return this.getNroContrato().equals(toCompare.getNroContrato());
    }


}
