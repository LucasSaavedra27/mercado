package mercado.contrato;

import java.util.ArrayList;

import mercado.cliente.ContratoExistenteException;
import mercado.cliente.ContratoInexistenteException;

public class RepositorioContrato {
    private ArrayList<Contrato> contratos;

    public RepositorioContrato() {
        this.contratos = new ArrayList<>();
    }

    public void setRepositorioContrato(ArrayList<Contrato> contratoList) {
        this.contratos = contratoList;
    }

    public ArrayList<Contrato> getRepositorioContrato() {
        return contratos;
    }

  public void agregarContrato(Contrato contrato) throws ContratoExistenteException{
        if (ContratoExistente(contrato)==false) {
            contratos.add(contrato);
        }else{
            throw new ContratoExistenteException("El contrato nro: "+contrato.getNroContrato()+" ya existe");
        }
    }

    private Boolean ContratoExistente(Contrato contrato){
        for (Contrato contratoAux : contratos) {
            if (contratoAux.equals(contrato)) {
                return true;
            }
        }
        return false;
    }

    public Contrato buscarContrato(int numContrato) throws ContratoInexistenteException{
        for (Contrato contratoAux : contratos) {
            if(contratoAux.getNroContrato().equals(numContrato)){
                return contratoAux;
            }
        }
        throw new ContratoInexistenteException("El contrato nro: "+numContrato+" no existe");
    }

    public void modificarContrato(Contrato nuevoContrato) throws ContratoInexistenteException{
        Contrato contrato = buscarContrato(nuevoContrato.getNroContrato());
        contratos.remove(contrato);
        contratos.add(nuevoContrato);
    }

    public void eliminarContrato(int nroContrato) throws ContratoInexistenteException{
        Contrato contrato = buscarContrato(nroContrato);
        contratos.remove(contrato);
    }


}
