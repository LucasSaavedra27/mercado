package mercado.lectura;

import java.time.LocalDate;

public class Lectura {
    private double consumo; 
    private Medidor medidor; 
    private LocalDate fechaLecturaInicial;

    
    public Lectura(double consumo, Medidor medidor, LocalDate fechaLecturaInicial) {
        this.consumo = consumo;
        this.medidor = medidor;
        this.fechaLecturaInicial = fechaLecturaInicial;
    }
    public double getConsumo() {
        return consumo;
    }
     public void setConsumo(double consumo) {
        this.consumo = consumo;
    }
     public Medidor getMedidor() {
        return medidor;
    }
    public void setMedidor(Medidor medidor) {
        this.medidor = medidor;
    }
     public LocalDate getFechaLecturaInicial() {
        return fechaLecturaInicial;
    }
     public void setFechaLecturaInicial(LocalDate fechaLecturaInicial) {
        this.fechaLecturaInicial = fechaLecturaInicial;
    }
    public boolean equals(Object o){
        Lectura aComparar=(Lectura)o;
        return this.getFechaLecturaInicial().equals(aComparar.getFechaLecturaInicial());
    }

}

