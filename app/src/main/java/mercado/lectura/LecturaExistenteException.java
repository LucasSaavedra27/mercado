package mercado.lectura;

public class LecturaExistenteException extends Exception {
    
        public LecturaExistenteException(String message) {
            super(message);
        }
    }
    
