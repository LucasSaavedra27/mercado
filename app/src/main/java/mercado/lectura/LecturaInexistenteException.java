package mercado.lectura;

public class LecturaInexistenteException extends Exception {
    public LecturaInexistenteException(String message) {
        super(message);
    }
    
}
