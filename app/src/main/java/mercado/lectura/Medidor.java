package mercado.lectura;
import java.time.LocalDate;

import java.util.ArrayList;

public class Medidor {
    private Integer nroMedidor; 
    ArrayList <Lectura> lecturas;
    
    public Medidor(Integer nroMedidor) {
        this.nroMedidor = nroMedidor;
        lecturas =new ArrayList<>();
        }

    public Integer getNroMedidor() {
        return nroMedidor;
    }
    public void setNroMedidor(Integer nroMedidor) {
        this.nroMedidor = nroMedidor;
    }
    public ArrayList<Lectura> getLecturas() {
        return lecturas;
    }
    public void setLecturas(ArrayList<Lectura> lecturas) {
        this.lecturas = lecturas;
    } 
     public boolean equals(Object o){
        Medidor aComparar=(Medidor)o;
        return this.getNroMedidor().equals(aComparar.getNroMedidor());
    }
    public void agregarLectura (Lectura nuevaLectura) throws LecturaExistenteException {
        if (lecturaExistente (nuevaLectura)==false) {
            lecturas.add(nuevaLectura); 
        }else {
            throw new LecturaExistenteException ("La lectura con fecha" + nuevaLectura.getFechaLecturaInicial()+ "ya existe" );}
        }
    public boolean lecturaExistente (Lectura nuevaLectura){
        for (Lectura lecturaAux : lecturas){
            if (lecturaAux.equals(nuevaLectura)){
                return true; }
             }
        return false;
        }
    public Lectura buscarLectura (LocalDate fechaLecturaInicial) throws LecturaInexistenteException{
        for (Lectura nuevalectura : lecturas){
            if (nuevalectura.getFechaLecturaInicial().equals(fechaLecturaInicial)){
                return nuevalectura; 
            }
        }
        throw new LecturaInexistenteException ("La lectura con fecha " + fechaLecturaInicial + " no existe" );
        }
     public void eliminarLectura (LocalDate fechaLecturaInicial) throws LecturaInexistenteException{
            Lectura nuevaLectura = buscarLectura(fechaLecturaInicial);
            lecturas.remove (nuevaLectura);

        }
    }
