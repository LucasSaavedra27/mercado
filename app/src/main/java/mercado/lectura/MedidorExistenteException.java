package mercado.lectura;

public class MedidorExistenteException extends Exception{
    public MedidorExistenteException(String message) {
        super(message);
    }
}
