package mercado.lectura;

public class MedidorInexistenteException extends Exception {
   
    public MedidorInexistenteException(String message) {
        super(message);
    }
}


