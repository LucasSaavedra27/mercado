package mercado.lectura;

import java.util.ArrayList;

public class RepositorioMedidores {
  private ArrayList <Medidor> medidores;

    public RepositorioMedidores() {
        medidores = new ArrayList<>();
    }

    public ArrayList<Medidor> getMedidores() {
        return medidores;
    }

    public void setNroMedidor(ArrayList<Medidor> medidores) {
        this.medidores = medidores;
    }
    public void agregarMedidor( Medidor medidor) throws MedidorExistenteException{
        if (buscarMedidorExistente (medidor)==false){
            medidores.add(medidor);
        }else {
            throw new MedidorExistenteException("El medidor "+medidor.getNroMedidor()+" ya existe");
        }
    }   
    private Boolean buscarMedidorExistente(Medidor medidor){
        for (Medidor medidorAux : medidores) {
            if (medidorAux.equals(medidor)) {
                return true;
            }
        }
        return false;
    }
    public Medidor buscarMedidor (Integer nroMedidor) throws MedidorInexistenteException{
        for (Medidor medidor: medidores) {
            if(medidor.getNroMedidor().equals(nroMedidor)){
                return medidor;
            }
        }
        throw new MedidorInexistenteException("El medidor "+nroMedidor+" no existe");
    }
    public void eliminarMedidor(Integer nroMedidor) throws MedidorInexistenteException{
        Medidor medidor = buscarMedidor(nroMedidor);
        medidores.remove(medidor);
    }

   
}
   


 
    

    
