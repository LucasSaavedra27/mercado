package mercado.sector;

import mercado.lectura.Medidor;

public class Puesto {
    private Integer nroPuesto;
    private Boolean disponibilidad;
    private Boolean techo;
    private Boolean camaraRefrigerante;
    private Double dimensionM2;
    private Double precio;
    private Medidor medidor;
    
    public Puesto(Integer nroPuesto, Boolean disponibilidad, Boolean techo, Boolean camaraRefrigerante, Double dimensionM2,
            Double precio, Medidor medidor) {
        this.nroPuesto = nroPuesto;
        this.disponibilidad = disponibilidad;
        this.techo = techo;
        this.camaraRefrigerante = camaraRefrigerante;
        this.dimensionM2 =dimensionM2;
        this.precio = precio;
        this.medidor = medidor;
    }

    public Integer getNroPuesto() {
        return nroPuesto;
    }

    public void setNroPuesto(Integer nroPuesto) {
        this.nroPuesto = nroPuesto;
    }

    public Boolean getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(Boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public Boolean getTecho() {
        return techo;
    }

    public void setTecho(Boolean techo) {
        this.techo = techo;
    }

    public Boolean getCamaraRefrigerante() {
        return camaraRefrigerante;
    }

    public void setCamaraRefrigerante(Boolean camaraRefrigerante) {
        this.camaraRefrigerante = camaraRefrigerante;
    }

    public Double getDimensionM2() {
        return dimensionM2;
    }

    public void setDimensionM2(Double dimensionM2) {
        this.dimensionM2 = dimensionM2;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Medidor getMedidor() {
        return medidor;
    }

    public void setMedidor(Medidor medidor) {
        this.medidor = medidor;
    }

    public boolean equals(Object o){
        Puesto aComparar=(Puesto)o;
        return this.getNroPuesto().equals(aComparar.getNroPuesto());
    }

    @Override
    public String toString() {
        return "nroPuesto=" + nroPuesto + ", disponibilidad=" + disponibilidad + ", techo=" + techo
                + ", camaraRefrigerante=" + camaraRefrigerante + ", dimensionM2=" + dimensionM2 + ", precio=" + precio
                + ", medidor=" + medidor;
    }
    
    
    
}
