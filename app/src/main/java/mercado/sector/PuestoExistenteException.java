package mercado.sector;

public class PuestoExistenteException extends Exception{
    public PuestoExistenteException(String message) {
        super(message);
    }
}
