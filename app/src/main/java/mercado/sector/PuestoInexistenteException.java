package mercado.sector;

public class PuestoInexistenteException extends Exception{
    public PuestoInexistenteException(String message) {
        super(message);
    }
}
