package mercado.sector;

import java.util.ArrayList;

public class RepositorioSector {
    private ArrayList<Sector> sectores;

    public RepositorioSector() {
        this.sectores = new ArrayList<Sector>();
    }

    public ArrayList<Sector> getSectores() {
        return sectores;
    }

    public void setSectores(ArrayList<Sector> sectores) {
        this.sectores = sectores;
    }
    
    public void agregarSector(Sector sector) throws SectorExistenteException{
        if (sectorExistente(sector)==false) {
            sectores.add(sector);
        }else{
            throw new SectorExistenteException("El sector "+sector.getNombre()+" ya existe");
        }
    }

    private Boolean sectorExistente(Sector sector){
        for (Sector sectorAux : sectores) {
            if (sectorAux.equals(sector)) {
                return true;
            }
        }
        return false;
    }

    public Sector buscarSector(String nombreSector) throws SectorInexistenteException{ //consultar al profesor si agrego un codigo de sector para comparar 
        for (Sector sector: sectores) {
            if(sector.getNombre().equals(nombreSector)){
                return sector;
            }
        }
        throw new SectorInexistenteException("El sector "+nombreSector+" no existe");
    }

    public void modificarSector(Sector nuevoSector) throws SectorInexistenteException{
        Sector sector = buscarSector(nuevoSector.getNombre());
        sectores.remove(sector);
        sectores.add(nuevoSector);
}

    public void eliminarSector(String nombreSector) throws SectorInexistenteException{
        Sector sector = buscarSector(nombreSector);
        sectores.remove(sector);
    }
   
}
