package mercado.sector;

import java.util.ArrayList;

public class Sector {
    private String nombre;
    private String zona;
    private ArrayList <Puesto> puestosDelSector;

    public Sector(String nombre, String zona) {
        this.nombre = nombre;
        this.zona = zona;
        puestosDelSector=new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public boolean equals(Object o){
        Sector aComparar=(Sector)o;
        return this.getNombre().equals(aComparar.getNombre());
    }

    public ArrayList<Puesto> getPuestosDelSector() {
        return puestosDelSector;
    }

    public void setPuestosDelSector(ArrayList<Puesto> puesto) {
        puestosDelSector = puesto;
    }

    public void agregarPuesto(Puesto puesto) throws PuestoExistenteException{
        if (puestoExistente(puesto)==false) {
            puestosDelSector.add(puesto);
        }else{
            throw new PuestoExistenteException("El puesto nro: "+puesto.getNroPuesto()+" ya existe");
        }
    }

    public Boolean puestoExistente(Puesto puesto){
        for (Puesto puestoAux : puestosDelSector) {
            if (puestoAux.equals(puesto)) {
                return true;
            }
        }
        return false;
    }
    
    public Puesto buscarPuesto(int numPuesto) throws PuestoInexistenteException{
        for (Puesto puesto : puestosDelSector) {
            if(puesto.getNroPuesto().equals(numPuesto)){
                return puesto;
            }
        }
        throw new PuestoInexistenteException("El nro de puesto: "+numPuesto+" no existe");
    }

    public void modificarPuesto(Puesto nuevoPuesto) throws PuestoInexistenteException{
            Puesto puesto = buscarPuesto(nuevoPuesto.getNroPuesto());
            puestosDelSector.remove(puesto);
            puestosDelSector.add(nuevoPuesto);
    }


    public void eliminarPuesto(int nroPuesto) throws PuestoInexistenteException{
        Puesto puesto = buscarPuesto(nroPuesto);
        puestosDelSector.remove(puesto);
    }


}
