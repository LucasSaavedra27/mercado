package mercado.sector;

public class SectorExistenteException extends Exception {
    public SectorExistenteException(String message) {
        super(message);
    }
}
