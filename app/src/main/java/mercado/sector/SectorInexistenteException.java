package mercado.sector;

public class SectorInexistenteException extends Exception{
    public SectorInexistenteException(String message) {
        super(message);
    }
}
