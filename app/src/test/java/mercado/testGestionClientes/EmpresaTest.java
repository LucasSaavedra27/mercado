package mercado.testGestionClientes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mercado.cliente.Empresa;

public class EmpresaTest {
    @Test
    public void creacionEmpresa(){
        Empresa empresa1=new Empresa("Catamarca", "emp@gmail.com", "154505040", "20-30303030-2", "S.A");
        String cuitEmpresa= empresa1.getCuit();
        assertEquals(cuitEmpresa, empresa1.getCuit());  
    }
    @Test
    public void dosEmpresasIguales(){
        Empresa empresa1=new Empresa("Catamarca", "emp@gmail.com", "154505040", "20-30303030-2", "S.A");
        Empresa empresa2=new Empresa("Catamarca", "emp@gmail.com", "154505040", "20-30303030-2", "S.A");
        boolean empresasIGuales=empresa1.equals(empresa2);
        assertTrue(empresasIGuales);
    }

}
