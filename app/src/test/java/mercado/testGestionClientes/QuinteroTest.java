package mercado.testGestionClientes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

import mercado.cliente.ContratoExistenteException;
import mercado.cliente.ContratoInexistenteException;
import mercado.cliente.Quintero;
import mercado.contrato.Contrato;
import mercado.lectura.Lectura;
import mercado.lectura.Medidor;
import mercado.sector.Puesto;


public class QuinteroTest {

    Medidor medidor = new Medidor(25);
    Lectura lectura = new Lectura(540, medidor, LocalDate.of(2024, 5, 6));
    Puesto puesto = new Puesto(1020, true, true, false, 15.0, 10000.0, medidor);

    @Test
    public void creacionQuintero(){
        Quintero quintero1=new Quintero("Catamarca", "quin@gmail.com", "154505040","Raul", "Cisterna", "27845321");
        String dni=quintero1.getDni();
        assertEquals(dni, quintero1.getDni());
    }
    @Test
    public void dosDniIguales(){
        Quintero quintero1=new Quintero("Catamarca", "quin@gmail.com", "154505040","Raul", "Cisterna", "27845321");
        Quintero quintero2=new Quintero("Catamarca", "quin@gmail.com", "154505040","Raul", "Cisterna", "27845321");
        boolean dniIguales=quintero1.equals(quintero2);
        assertTrue(dniIguales);
    }

    @Test
    public void agregarContrato() throws ContratoExistenteException{
        Quintero quintero1=new Quintero("Catamarca", "quin@gmail.com", "154505040","Raul", "Cisterna", "27845321");
        Contrato contrato1= new Contrato(10001, LocalDate.now(), LocalDate.of(2025, 1, 1), 150000.0,"Lautaro",quintero1,lectura,puesto);
        quintero1.agregarContrato(contrato1);
        assertEquals(1, quintero1.getContratos().size());
        
    }

    @Test
    public void buscarContratoExistente() throws ContratoExistenteException, ContratoInexistenteException{
        Quintero quintero1=new Quintero("Catamarca", "quin@gmail.com", "154505040","Raul", "Cisterna", "27845321");
        Contrato contrato1= new Contrato(10001, LocalDate.now(), LocalDate.of(2025, 1, 1), 150000.0,"Lautaro",quintero1,lectura,puesto);
        quintero1.agregarContrato(contrato1);
        Contrato contratoAux=quintero1.buscarContrato(10001);
        assertEquals(Integer.valueOf(10001), contratoAux.getNroContrato());
    }

    @Test (expected = ContratoExistenteException.class)
    public void contratoExistenteException() throws ContratoExistenteException{
        Quintero quintero1=new Quintero("Catamarca", "quin@gmail.com", "154505040","Raul", "Cisterna", "27845321");
        Contrato contrato1= new Contrato(10001, LocalDate.now(), LocalDate.of(2025, 1, 1), 150000.0,"Lautaro",quintero1,lectura,puesto);
        quintero1.agregarContrato(contrato1);
        quintero1.agregarContrato(contrato1);
    }

    @Test (expected = ContratoInexistenteException.class)
    public void contratoInexistenteException() throws ContratoInexistenteException{
        Quintero quintero1=new Quintero("Catamarca", "quin@gmail.com", "154505040","Raul", "Cisterna", "27845321");
        quintero1.buscarContrato(1);
    }

    @Test 
    public void modificarContrato() throws ContratoExistenteException, ContratoInexistenteException {
        Quintero quintero1=new Quintero("Catamarca", "quin@gmail.com", "154505040","Raul", "Cisterna", "27845321");
        Contrato contrato1= new Contrato(10001, LocalDate.now(), LocalDate.of(2025, 1, 1), 150000.0,"Lautaro",quintero1,lectura,puesto);
        Contrato contratoNuevo= new Contrato(10001, LocalDate.now(), LocalDate.of(2025, 1, 1), 150000.0,"Lautaro Perez",quintero1,lectura,puesto);
        quintero1.agregarContrato(contrato1);
        quintero1.modificarContrato(contratoNuevo);
        Contrato contrato=quintero1.buscarContrato(10001);
        assertEquals(LocalDate.of(2025, 1, 1), contrato.getFechaFin());
    }


    @Test
    public void eliminarContrato() throws ContratoExistenteException, ContratoInexistenteException{
        Quintero quintero1=new Quintero("Catamarca", "quin@gmail.com", "154505040","Raul", "Cisterna", "27845321");
        Contrato contrato1= new Contrato(10001, LocalDate.now(), LocalDate.of(2025, 1, 1), 150000.0,"Lautaro",quintero1,lectura,puesto);
        quintero1.agregarContrato(contrato1);
        quintero1.eliminarContrato(10001);
        assertEquals(0, quintero1.getContratos().size());
    }
}
