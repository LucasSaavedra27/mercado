package mercado.testGestionClientes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.cliente.Cliente;
import mercado.cliente.ClienteExistenteException;
import mercado.cliente.ClienteInexistenteException;
import mercado.cliente.Empresa;
import mercado.cliente.Quintero;
import mercado.cliente.RepositorioCliente;

public class RepositorioClienteTest {

    @Test
    public void agregarCliente() throws ClienteExistenteException {
        RepositorioCliente repo = new RepositorioCliente();
        Empresa empresa = new Empresa("Direccion Empresa", "empresa@mail.com", "154505040", "20304050607", "Razon Social SA");
        Quintero quintero1=new Quintero("Catamarca", "quin@gmail.com", "1545050402","Raul", "Cisterna", "27845321");
        repo.agregarCliente(empresa);
        repo.agregarCliente(quintero1);
        assertEquals(2, repo.getClientes().size());
    }

    @Test
    public void eliminarCliente() throws ClienteExistenteException, ClienteInexistenteException {
        RepositorioCliente repo = new RepositorioCliente();
        Empresa empresa = new Empresa("Direccion Empresa", "empresa@mail.com", "154505040", "20304050607", "Razon Social SA");
        Quintero quintero1=new Quintero("Catamarca", "quin@gmail.com", "1545050402","Raul", "Cisterna", "27845321");
        repo.agregarCliente(empresa);
        repo.agregarCliente(quintero1);
        repo.eliminarCliente("27845321");
        assertEquals(1, repo.getClientes().size());
    }

    @Test
    public void buscarClienteExistente() throws ClienteExistenteException, ClienteInexistenteException {
        RepositorioCliente repo = new RepositorioCliente();
        Empresa empresa = new Empresa("Direccion Empresa", "empresa@mail.com", "154505040", "20304050607", "Razon Social SA");
        repo.agregarCliente(empresa);
        assertEquals(empresa,repo.buscarCliente("20304050607"));
    }

    @Test (expected = ClienteInexistenteException.class)
    public void buscarClienteInexistente() throws ClienteExistenteException, ClienteInexistenteException {
        RepositorioCliente repo = new RepositorioCliente();
        repo.buscarCliente("20304050607");
    }

    @Test
    public void modificarCliente() throws ClienteExistenteException, ClienteInexistenteException {
        RepositorioCliente repo = new RepositorioCliente();
        Quintero quintero1 = new Quintero("Direccion Quintero", "quintero@mail.com", "1545050402", "Raul", "Cisterna", "27845321");
        Quintero quintero1nuevo = new Quintero("Direccion Quintero", "quintero@mail.com", "1545050402", "Raul Angel", "Cisterna", "27845321");
        repo.agregarCliente(quintero1);
        repo.modificarCliente(quintero1nuevo);
        Cliente clienteAux=repo.buscarCliente("27845321");
        assertEquals(quintero1nuevo,clienteAux);
    }

    @Test (expected = ClienteExistenteException.class)
    public void clienteExistenteException() throws ClienteExistenteException{
        RepositorioCliente repo = new RepositorioCliente();
        Quintero quintero1 = new Quintero("Direccion Quintero", "quintero@mail.com", "1545050402", "Raul", "Cisterna", "27845321");
        repo.agregarCliente(quintero1);
        repo.agregarCliente(quintero1);
       
    }

    @Test (expected = ClienteInexistenteException.class)
    public void clienteInexistenteException() throws ClienteInexistenteException{
        RepositorioCliente repo = new RepositorioCliente();
        repo.eliminarCliente("43923665");
    }

}

