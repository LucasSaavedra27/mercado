package mercado.testGestionContrato;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

import mercado.cliente.Cliente;
import mercado.cliente.Quintero;
import mercado.contrato.Contrato;
import mercado.lectura.Lectura;
import mercado.lectura.Medidor;
import mercado.sector.Puesto;

public class ContratoTest {
    Medidor medidor = new Medidor(25);
    Cliente cliente = new Quintero("Catamarca", "quin@gmail.com", "154505040","Raul", "Cisterna", "27845321");
    Lectura lectura = new Lectura(540, medidor, LocalDate.of(2024, 5, 6));
    Puesto puesto = new Puesto(1020, true, true, false, 15.0, 10000.0, medidor);

    @Test

    public void createContractTest() {
        Contrato contrato1 = new Contrato(1, LocalDate.now(), LocalDate.now(), 10.00, "javier", cliente, lectura,puesto);
        Integer numberContract = contrato1.getNroContrato();
        assertEquals(numberContract, contrato1.getNroContrato());
    }

    @Test
    public void compareContractTest(){
        Contrato contrato1 = new Contrato(1, LocalDate.now(), LocalDate.now(), 10.00, "javier", cliente, lectura, puesto);
        Contrato contrato2 = new Contrato(1, LocalDate.now(), LocalDate.now(), 10.00, "javier",cliente, lectura, puesto);
        Boolean equalsContract = contrato1.equals(contrato2);
        assertTrue(equalsContract);

    }

}
