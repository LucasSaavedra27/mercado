package mercado.testGestionContrato;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import org.junit.Test;

import mercado.cliente.ContratoExistenteException;
import mercado.cliente.ContratoInexistenteException;
import mercado.cliente.Quintero;
import mercado.contrato.Contrato;
import mercado.contrato.RepositorioContrato;
import mercado.lectura.Lectura;
import mercado.lectura.Medidor;
import mercado.sector.Puesto;

public class RepositorioContratoTest {

    Medidor medidor = new Medidor(25);
    Quintero cliente=new Quintero("Catamarca", "quin@gmail.com", "154505040","Raul", "Cisterna", "27845321");
    Lectura lectura = new Lectura(540, medidor, LocalDate.of(2024, 5, 6));
    Puesto puesto = new Puesto(1020, true, true, false, 15.0, 10000.0, medidor);

    @Test(expected = ContratoExistenteException.class)
    public void agregarContratoExistenteRepositorioTest() throws ContratoExistenteException {
        Contrato contrato1 = new Contrato(1, LocalDate.now(), LocalDate.now(), 10.00, "javier", cliente, lectura,puesto);
        RepositorioContrato repositorio = new RepositorioContrato();
        repositorio.agregarContrato(contrato1);
        repositorio.agregarContrato(contrato1);
    }

    @Test
    public void agregarContratoRepositorioTest() throws ContratoExistenteException {
        Contrato contrato1 = new Contrato(1, LocalDate.now(), LocalDate.now(), 10.00, "javier", cliente, lectura,puesto);
        RepositorioContrato repositorio = new RepositorioContrato();
        repositorio.agregarContrato(contrato1);
        assertEquals(1, repositorio.getRepositorioContrato().size());
    }

    @Test(expected = ContratoInexistenteException.class)
    public void buscarInexistenteContratoTest() throws ContratoInexistenteException {
        RepositorioContrato repository = new RepositorioContrato();
        repository.buscarContrato(2);
    }

    @Test
    public void buscarContratoTest() throws ContratoInexistenteException, ContratoExistenteException{
        Contrato contrato1 = new Contrato(1, LocalDate.now(), LocalDate.now(), 10.00, "javier", cliente, lectura,puesto);
        RepositorioContrato repository = new RepositorioContrato();
        repository.agregarContrato(contrato1);
        Contrato contradoBuscado = repository.buscarContrato(contrato1.getNroContrato());
        boolean expectResult = contradoBuscado.equals(contrato1);
        assertTrue(expectResult);
    }

    @Test(expected = ContratoInexistenteException.class)
    public void borrarInexistenteContratoTest() throws ContratoInexistenteException,ContratoExistenteException {
        RepositorioContrato repository = new RepositorioContrato();
        repository.eliminarContrato(2);
    }

    @Test
    public void borrarContratoTest() throws ContratoInexistenteException, ContratoExistenteException{
        Contrato contrato1 = new Contrato(1, LocalDate.now(), LocalDate.now(), 10.00, "javier", cliente, lectura,puesto);
        RepositorioContrato repository = new RepositorioContrato();
        repository.agregarContrato(contrato1);
        repository.eliminarContrato(contrato1.getNroContrato());
        assertEquals(0, repository.getRepositorioContrato().size());
    }

    @Test(expected = ContratoInexistenteException.class)
    public void modificarInexistenteContratoTest() throws ContratoInexistenteException, ContratoExistenteException{
        Contrato contrato1 = new Contrato(1, LocalDate.now(), LocalDate.now(), 10.00, "javier", cliente, lectura,puesto);
        RepositorioContrato repository = new RepositorioContrato();
        repository.modificarContrato(contrato1);
    }

    @Test
    public void modificarContratoTest() throws ContratoInexistenteException,ContratoExistenteException {
        Contrato contrato1 = new Contrato(1, LocalDate.now(), LocalDate.now(), 10.00, "javier", cliente, lectura,puesto);
        Contrato contratoParaModificar = new Contrato(1, LocalDate.now(), LocalDate.now(), 11.00, "javier", cliente,lectura, puesto);
        RepositorioContrato repository = new RepositorioContrato();
        repository.agregarContrato(contrato1);
        repository.modificarContrato(contratoParaModificar);
        Contrato contratoModificado = repository.buscarContrato(contratoParaModificar.getNroContrato());
        assertTrue(contratoModificado.getMontoMensual() != contrato1.getMontoMensual());
    }

}
