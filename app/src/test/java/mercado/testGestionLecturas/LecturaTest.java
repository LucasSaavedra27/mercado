package mercado.testGestionLecturas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

import mercado.lectura.Medidor;
import mercado.lectura.Lectura;

public class LecturaTest {
     @Test
    public void creacionLectura(){
        Medidor medidor=new Medidor(25);
        Lectura lectura =new Lectura(540, medidor,LocalDate.of(2024, 5, 6) );
        LocalDate fecha=lectura.getFechaLecturaInicial();
        assertEquals(LocalDate.of(2024, 5, 6), fecha);
    }
    @Test
     public void dosLecturasIguales(){
        Medidor medidor=new Medidor (25);
        Lectura lectura =new Lectura(540, medidor,LocalDate.of(2024, 5, 6) );
        Lectura lectura2 =new Lectura(540, medidor,LocalDate.of(2024, 5, 6) );
        boolean fechasIguales=lectura.equals(lectura2);
        assertTrue(fechasIguales);
    }
}
