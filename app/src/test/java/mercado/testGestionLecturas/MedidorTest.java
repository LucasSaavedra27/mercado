package mercado.testGestionLecturas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

import mercado.lectura.Lectura;
import mercado.lectura.LecturaExistenteException;
import mercado.lectura.LecturaInexistenteException;
import mercado.lectura.Medidor;



public class MedidorTest {
     @Test
    public void creacionMedidor(){
        Medidor medidor=new Medidor(25);
        Integer nroMedidor=medidor.getNroMedidor();
        Integer prueba = 25;
         assertEquals(prueba, nroMedidor);
}
    @Test
     public void dosMedidoresIguales(){
        Medidor medidor=new Medidor(25);
        Medidor medidor2=new Medidor(25);
       
        boolean medidoresIguales=medidor.equals(medidor2);
        assertTrue(medidoresIguales);
    }
     @Test 
    public void agregarLectura() throws LecturaExistenteException{
        Medidor medidor1 = new Medidor (25); 
        Lectura nuevaLectura1= new Lectura(658, medidor1, LocalDate.of(2024,05,23 )); 
        medidor1.agregarLectura(nuevaLectura1);
        assertEquals (1,medidor1.getLecturas().size()); 
    }
    @Test
    public void buscarLecturaExistente() throws LecturaExistenteException, LecturaInexistenteException {
        Medidor medidor1 = new Medidor (25); 
        Lectura nuevaLectura1= new Lectura(658, medidor1, LocalDate.of(2024,05,23 )); 
        medidor1.agregarLectura(nuevaLectura1);
        Lectura lecturaAux=medidor1.buscarLectura(LocalDate.of (2024,05,23));
        assertEquals(LocalDate.of(2024, 05,23), lecturaAux.getFechaLecturaInicial());
    }
    @Test (expected = LecturaExistenteException.class)
    public void LecturaExistenteException() throws LecturaExistenteException{
        Medidor medidor1 = new Medidor(25); 
        Lectura lectura1 = new Lectura(458, medidor1, LocalDate.of(2024,05,23));
        medidor1.agregarLectura(lectura1);
        medidor1.agregarLectura(lectura1);
    }
    @Test (expected = LecturaInexistenteException.class)
    public void LecturaInexistenteException () throws LecturaInexistenteException{
        Medidor medidor1 = new Medidor (25); 
        medidor1.buscarLectura(LocalDate.of (2024,05,23)); 
    }
    @Test 
    public void eliminarLectura()throws LecturaExistenteException, LecturaInexistenteException{
        Medidor medidor1 = new Medidor(25); 
        Lectura nuevaLectura1 = new Lectura(589, medidor1, LocalDate.of(2024,05,23));
        medidor1.agregarLectura(nuevaLectura1);
        medidor1.eliminarLectura(LocalDate.of (2024,05,23));
        assertEquals(0, medidor1.getLecturas().size());
    }
     }


   
  





