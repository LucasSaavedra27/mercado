package mercado.testGestionLecturas;
import org.junit.Test;
import mercado.lectura.Medidor;
import mercado.lectura.MedidorExistenteException;
import mercado.lectura.MedidorInexistenteException;
import mercado.lectura.RepositorioMedidores;


import static org.junit.Assert.assertEquals;


public class RepositorioMedidorTest {
      @Test
    public void agregarMedidor() throws MedidorExistenteException{
        Medidor medidor1 = new Medidor (25);
        RepositorioMedidores repositorioMedidor1=new RepositorioMedidores();
       repositorioMedidor1.agregarMedidor(medidor1);
        assertEquals(1, repositorioMedidor1.getMedidores().size());
    }
    @Test 
    public void  buscarMedidor() throws MedidorExistenteException, MedidorInexistenteException {
        Medidor medidor1 = new Medidor(25); 
        RepositorioMedidores repositorioMedidor1  = new RepositorioMedidores(); 
        repositorioMedidor1.agregarMedidor(medidor1);
        Medidor medidorAux= repositorioMedidor1.buscarMedidor(25); 
        assertEquals(Integer.valueOf (25),medidorAux.getNroMedidor()); }
    
        @Test (expected = MedidorExistenteException.class)
    public void medidorExistenteException() throws MedidorExistenteException{
        Medidor medidor1=new Medidor(25);
        RepositorioMedidores repositorioMedidor1=new RepositorioMedidores();
        repositorioMedidor1.agregarMedidor(medidor1);
        repositorioMedidor1.agregarMedidor(medidor1);
    }
    @Test (expected = MedidorInexistenteException.class)
    public void MedidorInexistenteException() throws MedidorInexistenteException{
       // Medidor medidor1 =new Medidor(25);
        RepositorioMedidores repositorioMedidor1=new RepositorioMedidores();
        repositorioMedidor1.buscarMedidor(28);}

    @Test
    public void eliminarMedidor() throws MedidorExistenteException, MedidorInexistenteException{
        Medidor medidor1=new Medidor(25);
        RepositorioMedidores repositorioMedidor1=new RepositorioMedidores();
        repositorioMedidor1.agregarMedidor(medidor1);
        repositorioMedidor1.eliminarMedidor(25);
        assertEquals(0, repositorioMedidor1.getMedidores().size());
    }
}

    



        


    


   