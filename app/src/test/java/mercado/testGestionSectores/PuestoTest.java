package mercado.testGestionSectores;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mercado.sector.Puesto;
import mercado.lectura.Medidor;

public class PuestoTest {
    @Test
    public void creacionPuesto(){
        Medidor medidor=new Medidor(25);
        Puesto puesto=new Puesto(1020, true, true, false, 15.0, 10000.0, medidor);
        Integer nropuesto=puesto.getNroPuesto();
        Integer valor=1020;
        assertEquals(valor, nropuesto);
    }
    @Test
    public void dosPuestosIguales(){
        Medidor medidor=new Medidor(25);
        Puesto puesto1=new Puesto(1020, true, true, false, 15.0, 10000.0, medidor);
        Puesto puesto2=new Puesto(1020, true, true, false, 15.0, 10000.0, medidor);
        boolean puestosIguales=puesto1.equals(puesto2);
        assertTrue(puestosIguales);
    }
}
