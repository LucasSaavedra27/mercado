package mercado.testGestionSectores;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.sector.RepositorioSector;
import mercado.sector.Sector;
import mercado.sector.SectorExistenteException;
import mercado.sector.SectorInexistenteException;

public class RepositorioSectorTest {
     @Test
    public void agregarSector() throws SectorExistenteException{
        Sector sector1=new Sector("VERDURAS", "NORTE");
        RepositorioSector repo=new RepositorioSector();
        repo.agregarSector(sector1);
        assertEquals(1, repo.getSectores().size());
        
    }

    @Test
    public void buscarSectorExistente() throws SectorExistenteException, SectorInexistenteException{
        Sector sector1=new Sector("VERDURAS", "NORTE");
        RepositorioSector repo=new RepositorioSector();
        repo.agregarSector(sector1);
        Sector sectorAux=repo.buscarSector("VERDURAS");
        assertEquals("VERDURAS", sectorAux.getNombre());
    }

    @Test (expected = SectorExistenteException.class)
    public void sectorExistenteException() throws SectorExistenteException{
        Sector sector1=new Sector("VERDURAS", "NORTE");
        RepositorioSector repo=new RepositorioSector();
        repo.agregarSector(sector1);
        repo.agregarSector(sector1);
    }

    @Test (expected = SectorInexistenteException.class)
    public void sectorInexistenteException() throws SectorInexistenteException{
        RepositorioSector repo=new RepositorioSector();
        repo.buscarSector("FRUTAS");
    }

    @Test 
    public void modificarSector() throws SectorExistenteException, SectorInexistenteException {
        RepositorioSector repo=new RepositorioSector();
        Sector sector1=new Sector("VERDURAS", "NORTE");
        Sector sectorNuevo=new Sector("VERDURAS", "NORTE-CENTRO");
        repo.agregarSector(sector1);
        repo.modificarSector(sectorNuevo);
        Sector sector=repo.buscarSector("VERDURAS");
        assertEquals("NORTE-CENTRO", sector.getZona());
    }


    @Test
    public void eliminarSector() throws SectorExistenteException, SectorInexistenteException{
        Sector sector1=new Sector("VERDURAS", "NORTE");
        RepositorioSector repo=new RepositorioSector();
        repo.agregarSector(sector1);
        repo.eliminarSector("VERDURAS");
        assertEquals(0, repo.getSectores().size());
    }
}
