package mercado.testGestionSectores;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mercado.lectura.Medidor;
import mercado.sector.Puesto;
import mercado.sector.PuestoExistenteException;
import mercado.sector.PuestoInexistenteException;
import mercado.sector.Sector;

public class SectorTest {
    @Test
    public void creacionSector(){
        Sector sector=new Sector("VERDURAS", "NORTE");
        String nombre=  sector.getNombre();
        assertEquals("VERDURAS", nombre); 
    }

    @Test
    public void dosSectoresIguales(){
        Sector sector1=new Sector("VERDURAS", "NORTE");
        Sector sector2=new Sector("VERDURAS", "NORTE");
        boolean sectoresIguales= sector1.equals(sector2);
        assertTrue(sectoresIguales);
    }

    @Test
    public void agregarPuesto() throws PuestoExistenteException{
        Sector sector1=new Sector("VERDURAS", "NORTE");
        Medidor medidor1=new Medidor(25);
        Puesto puesto1=new Puesto(1020, true, true, false, 15.0, 10000.0, medidor1);
        sector1.agregarPuesto(puesto1);
        assertEquals(1, sector1.getPuestosDelSector().size());
        
    }

    @Test
    public void buscarPuestoExistente() throws PuestoExistenteException, PuestoInexistenteException{
        Sector sector1=new Sector("VERDURAS", "NORTE");
        Medidor medidor1=new Medidor(25);
        Puesto puesto1=new Puesto(1020, true, true, false, 15.0, 10000.0, medidor1);
        sector1.agregarPuesto(puesto1);
        Puesto puestoAux=sector1.buscarPuesto(1020);
        assertEquals(Integer.valueOf(1020), puestoAux.getNroPuesto());
    }

    @Test (expected = PuestoExistenteException.class)
    public void puestoExistenteException() throws PuestoExistenteException{
        Sector sector1=new Sector("VERDURAS", "NORTE");
        Medidor medidor1=new Medidor(25);
        Puesto puesto1=new Puesto(1020, true, true, false, 15.0, 10000.0, medidor1);
        sector1.agregarPuesto(puesto1);
        sector1.agregarPuesto(puesto1);
    }

    @Test (expected = PuestoInexistenteException.class)
    public void puestoInexistenteException() throws PuestoInexistenteException{
        Sector sector1=new Sector("VERDURAS", "NORTE");
        sector1.buscarPuesto(1020);
    }

    @Test 
    public void modificarPuesto() throws PuestoExistenteException, PuestoInexistenteException {
        Sector sector1=new Sector("VERDURAS", "NORTE");
        Medidor medidor1=new Medidor(25);
        Puesto puesto1=new Puesto(1020, true, true, false, 15.0, 10000.0, medidor1);
        Puesto puestoNuevo=new Puesto(1020, true, true, false, 15.0, 15500.99, medidor1);
        sector1.agregarPuesto(puesto1);
        sector1.modificarPuesto(puestoNuevo);
        Puesto puesto=sector1.buscarPuesto(1020);
        assertEquals(Double.valueOf(15500.99), puesto.getPrecio());
    }


    @Test
    public void eliminarPuesto() throws PuestoExistenteException, PuestoInexistenteException{
        Sector sector1=new Sector("VERDURAS", "NORTE");
        Medidor medidor1=new Medidor(25);
        Puesto puesto1=new Puesto(1020, true, true, false, 15.0, 10000.0, medidor1);
        sector1.agregarPuesto(puesto1);
        sector1.eliminarPuesto(1020);
        assertEquals(0, sector1.getPuestosDelSector().size());
    }


}
